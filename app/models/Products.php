<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [

    	'nama','code','wastepointid','harga','aktif'

    ];

    public function wastepoint()
  	{
      return $this->hasOne('App\models\Wastepoint', 'id','wastepointid')->first();
  	}

    public function materials()
    {
      return $this->hasMany('App\models\Materials', 'productid','code');
    }
}
