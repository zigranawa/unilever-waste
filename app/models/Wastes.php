<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Wastes extends Model
{
    protected $fillable = [
      'wastepointid','productid','status','errorid','start_date','finish_date','total_hours','isFull','created_by',
      'finished_by'
    ];
}
