<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Wastepoint extends Model
{
    protected $fillable = [
    	'nama','code','mesinid','aktif'
    ];

    public function mesin()
  	{
      return $this->hasOne('App\models\Mesin', 'id','mesinid')->first();
  	}

    public function product()
    {
      return $this->hasMany('App\models\Products', 'wastepointid','id');
    }

}
