<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model
{
    protected $fillable = [
      'code','productid','nama_material','type','Currency','umb','priceunit','valueprice','aktif'

    ];
}
