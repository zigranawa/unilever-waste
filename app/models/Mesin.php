<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Mesin extends Model
{
    protected $fillable = [
    	'nama_mesin','aktif'
    ];
}
