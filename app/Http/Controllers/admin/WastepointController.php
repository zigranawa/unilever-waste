<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\WastepointRepository;

class WastepointController extends Controller
{
    protected $wastepoint;

    public function __construct(WastepointRepository $wastepoint)
    {
        $this->wastepoint = $wastepoint;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $where = array('aktif'=>1);
        $data = $this->wastepoint->lists($where);

        return view('admin.pages.wastepoint.lists',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mesin = $this->wastepoint->mesin();

        $data = array();
        $data['mesin'] = $mesin;

        return view('admin.pages.wastepoint.tambah',['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator =[
            'nama' => 'required|string',
            'code' => 'required|string',
            'mesin' => 'required|integer',
        ];

        $message = [
            'nama.required' => 'Nama wastepoint belum diisi',
            'code.required' => 'Code wastepoint belum diisi',
            'mesin.required' => 'Nama mesin belum dipilih',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama'      => $input['nama'],
            'code'      => $input['code'],
            'mesinid'   => $input['mesin'],
            'aktif'     => 1
        );

        $tambah = $this->wastepoint->simpan($paramsData);

        return redirect()->route('wastepoint.index')->with('success','Wastepoint berhasil ditambahkan');
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->wastepoint->show($id);

        return view('admin.pages.wastepoint.detail',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->wastepoint->show($id);
        
        $mesin = $this->wastepoint->mesin();

        $data->mesin = $mesin;
        
        return view('admin.pages.wastepoint.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator =[
            'nama' => 'required|string'
        ];

        $message = [
            'nama.required' => 'Nama wastepoint belum diisi',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama'      => $input['nama']
        );

        $edit = $this->wastepoint->edit($paramsData,$id);

        return redirect()->route('wastepoint.index')->with('success','Wastepoint berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = $this->wastepoint->hapus($id);

        return redirect()->route('wastepoint.index')->with('success','Wastepoint berhasil dihapus');
    }
}
