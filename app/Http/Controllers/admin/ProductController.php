<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\ProductRepository;

class ProductController extends Controller
{

    protected $product;

    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $where = array('aktif'=>1);
        $data = $this->product->lists($where);

        return view('admin.pages.product.lists',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wastepoint = $this->product->wastepoint();

        $data = array();
        $data['wastepoint'] = $wastepoint;

        return view('admin.pages.product.tambah',['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator =[
            'nama' => 'required|string',
            'code' => 'required|string',
            'wastepoint' => 'required|integer',
            'harga' => 'required|integer',
        ];

        $message = [
            'nama.required'         => 'Nama Product belum diisi',
            'code.required'         => 'Code Product belum diisi',
            'wastepoint.required'   => 'Nama wastepoint belum dipilih',
            'harga.required'        => 'Nama harga belum diisi',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama'      => $input['nama'],
            'code'      => $input['code'],
            'wastepointid'   => $input['wastepoint'],
            'harga'   => $input['harga'],
            'aktif'     => 1
        );

        $tambah = $this->product->simpan($paramsData);

        return redirect()->route('product.index')->with('success','Product berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->product->show($id);
        
        return view('admin.pages.product.detail',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->product->show($id);
        
        $wastepoint = $this->product->wastepoint();

        $data->wastepoint = $wastepoint;
        
        return view('admin.pages.product.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator =[
            'nama' => 'required|string'
        ];

        $message = [
            'nama.required' => 'Nama product belum diisi',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama'      => $input['nama']
        );

        $edit = $this->product->edit($paramsData,$id);

        return redirect()->route('product.index')->with('success','Product berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = $this->product->hapus($id);

        return redirect()->route('product.index')->with('success','Product berhasil dihapus');
    }
}
