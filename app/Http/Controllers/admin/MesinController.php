<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\MesinRepository;

class MesinController extends Controller
{

    protected $mesin;

    public function __construct(MesinRepository $mesin)
    {
        $this->mesin = $mesin;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $where = array('aktif'=>1);
        $data = $this->mesin->lists($where);

        return view('admin.pages.mesin.lists',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.mesin.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator =[
            'nama_mesin' => 'required|string',
        ];

        $message = [
            'nama_mesin.required' => 'Nama mesin belum diisi',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama_mesin'      => $input['nama_mesin'],
            'aktif'     => 1
        );

        $tambah = $this->mesin->simpan($paramsData);

        return redirect()->route('mesin.index')->with('success','Mesin berhasil ditambahkan');
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->mesin->show($id);
        
        return view('admin.pages.mesin.detail',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->mesin->show($id);
        
        return view('admin.pages.mesin.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator =[
            'nama_mesin' => 'required|string'
        ];

        $message = [
            'nama_mesin.required' => 'Nama mesin belum diisi',
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'nama_mesin'      => $input['nama_mesin']
        );

        $edit = $this->mesin->edit($paramsData,$id);

        return redirect()->route('mesin.index')->with('success','Mesin berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = $this->mesin->hapus($id);

        return redirect()->route('mesin.index')->with('success','Mesin berhasil dihapus');
    }
}
