<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use App\Repository\UsersRepository;

class UserController extends Controller
{
    
    protected $users;

    public function __construct(UsersRepository $users)
    {
        $this->users = $users;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $where = array('Aktif'=>1);
        $data = $this->users->lists($where);

        return view('admin.pages.user.lists',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.pages.user.tambah',['data'=>[]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator =[
            'name' => 'required|string',
            'email'=> 'required|email|string|unique:users',
            'password' => 'required|required_with:konfirmasi_password',
            'konfirmasi_password' => 'required|same:password'
        ];

        $message = [
            'name.required' => 'Nama belum diisi',
            'password.required' => 'Password belum diisi',
            'email.required' => 'Email belum diisi',
            'email.unique'   => 'Email sudah terdaftar',
            'konfirmasi_password.required' => 'Konfirmasi Password belum diisi',
            'konfirmasi_password.same' => 'Konfirmasi Password tidak sama'
        ];

        $validatedData = $request->validate($validator,$message);
        
        $paramsData= array(
            'name'      => $input['name'],
            'email'     => $input['email'],
            'role'      => $input['role'],
            'Aktif'     => 1
        );

        if(!empty($input['password'])){
            $paramsData['password'] = Hash::make($input['password']);
        }

        $tambah = $this->users->simpan($paramsData);

        return redirect()->route('user.index')->with('success','User berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->users->show($id);
        
        return view('admin.pages.user.detail',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->users->show($id);
        
        return view('admin.pages.user.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $validator =[
            'name' => 'required|string',
            'email'=> 'required|email|string|unique:users,email,'.$id,
            'password' => 'required_with:konfirmasi_password|same:konfirmasi_password'
        ];

        $message = [
            'name.required' => 'Nama belum diisi',
            'password.required' => 'Password belum diisi',
            'email.required' => 'Email belum diisi',
            'email.unique'   => 'Email sudah terdaftar',
            'password.same' =>  'Konfirmasi Password tidak sama'
        ];

        $validatedData = $request->validate($validator,$message);

        $paramsData= array(
            'name'      => $input['name'],
            'email'     => $input['email'],
            'role'     => $input['role'],
        );

        if(!empty($input['password'])){
            $paramsData['password'] = Hash::make($input['password']);
        }

        $update = $this->users->edit($paramsData,$id);

        return redirect()->route('user.index')->with('success','User berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = $this->users->hapus($id);

        return redirect()->route('user.index')->with('success','User berhasil dihapus');
    }
}
