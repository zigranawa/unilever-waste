<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Repository\ReportRepository;

class LaporanController extends Controller
{
    
    protected $report;

    public function __construct(ReportRepository $report)
    {
        $this->report = $report;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.report.lists',['data'=>[]]);
    }

}
