<?php

namespace App\Http\Controllers\api\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request){ 

        $validator = Validator::make($request->all(), [
            'kode' => 'required|string|max:6',
            'password' => 'required|string'
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()], 422);
        }

        // $user = User::where([

        //     'kode' => $request->kode,

        // ])->first();
        
        // if ($user) {
            
        //     $success['token'] =  $user->createToken('ICWMS')->accessToken; 

        //     return response()->json(['success' => $success], $this->successStatus); 

        // } else {
            
        //     return response()->json(['error'=>'Unauthorised'], 401); 

        // }

    	if(Auth::attempt(['kode' => request('kode'), 'password' => request('password')])){ 

    		$user = Auth::user(); 
    
    		$success['token'] =  $user->createToken('ICWMS')->accessToken; 
    
    		return response()->json(['success' => $success], $this->successStatus); 
    
    	} 
    
    	else{ 
    
    		return response()->json(['error'=>'Unauthorised'], 401); 
    
    	} 
    
    }


    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
    	$validator = Validator::make($request->all(), [ 
    		'name' => 'required', 
    		'email' => 'required|email', 
    		'password' => 'required', 
    		'c_password' => 'required|same:password', 
    	]);
    	if ($validator->fails()) { 
    		return response()->json(['error'=>$validator->errors()], 401);            
    	}
    	$input = $request->all(); 
    	$input['password'] = bcrypt($input['password']); 
        $input['Aktif'] = 1;
        $input['role'] = 1;
        $input['kode'] = 123456;
        
    	$user = User::create($input); 
    	$success['token'] =  $user->createToken('MyApp')->accessToken; 
    	$success['name'] =  $user->name;
    	return response()->json(['success'=>$success], $this->successStatus); 
    }

    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['data' => $user], $this->successStatus); 
    } 
}
