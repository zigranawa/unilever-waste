<?php

namespace App\Http\Controllers\api\scan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repository\WastebinRepository;

class WastebinController extends Controller
{
    protected $wastebin;

    public function __construct(WastebinRepository $wastebin)
    {
        $this->wastebin = $wastebin;
    }

    public function lists(Request $request){

        $input = $request->all();

        return response()->json(['data' => $input,'message'=>'Success get data list'], 200); 

    }

    public function check(Request $request){

        $input = $request->all();

        $where = array('code'=>$input['qrcode']);

        $data = $this->wastebin->show($where);

        return response()->json(['data' => $data,'message'=>'Success get data'], 200); 

    }

    public function errorlist(Request $request){

        $input = $request->all();

        $data = $this->wastebin->errorlist();

        return response()->json(['data' => $data,'message'=>'Success get data error'], 200); 

    }

    public function collect(Request $request){

        $input = $request->all();

        $data = $this->wastebin->collect($input);

        $message = !empty($data) ? 'Success collect data' : 'Data sudah di college';

        return response()->json(['data' => $data,'message'=>$message], 200); 

    }

    public function full(Request $request){

        $input = $request->all();

        $data = $this->wastebin->full($input);

        $message = !empty($data) ? 'Success full' : 'Data tidak ditemukan';

        return response()->json(['data' => $data,'message'=>$message], 200); 

    }


}
