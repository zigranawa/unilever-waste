<?php

namespace App\Repository;

use App\Repository\ProductInterface;

use Illuminate\Support\Str;

use App\models\Wastepoint;
use App\models\Products;

class ProductRepository implements ProductInterface
{
    
    public function lists($params){

        $data = Products::select('*')->where($params)->orderBy('id', 'desc')->get();

        foreach ($data as $key => $value) {
            $westpoint = $value->wastepoint();

            $data[$key]->nama_wastepoint = $westpoint->nama ?? '';

        }

        return $data;


    }
    
    public function simpan($params){

        $tambah = Products::create($params);

        return $tambah;

    }
    
    public function show($id){

        $data = Products::find($id);
        $wastepoint = $data->wastepoint();

        $data->nama_wastepoint = $wastepoint->nama;

        return $data;

    }
    
    public function edit($params,$id){

        $update = Products::where('id',$id)->update($params);
        
        return $update;

    }

    public function hapus($id) {

        $params = array('aktif'=> 0);
        $delete = Products::where('id',$id)->update($params);

        return $delete;
    }

    public function wastepoint(){
        $mesin = Wastepoint::select()->where(array('aktif' => 1))->get();

        return $mesin;
    }
}