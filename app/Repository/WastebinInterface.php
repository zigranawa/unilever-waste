<?php

namespace App\Repository;

Interface WastebinInterface {
    public function lists($params);
    public function show($params);
}