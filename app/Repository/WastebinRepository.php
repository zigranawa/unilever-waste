<?php

namespace App\Repository;

use App\Repository\WastebinInterface;

use Illuminate\Support\Str;

use App\models\Wastes;

use App\models\Wastepoint;

class WastebinRepository implements WastebinInterface
{
    
    public function lists($where){

        $data = Wastes::select('*')->where($where)->orderBy('id', 'desc')->get();

        return $data;


    }
    
    public function show($where){

        $data = Wastepoint::select('*')->where($where)->first();

        $dataproduct = $data->product()->select('nama','code')->get();

        foreach ($dataproduct as $key => $value) {

            $dataMaterial = $value->materials()->select('nama_material','code','type','priceunit')->get();

            $dataproduct[$key]->material = $dataMaterial;
        }

        $data->product = $dataproduct;

        return $data;

    }

    function checkdata($where){

        $data = Wastes::where($where)->first();

        return $data;
    }

    public function errorlist()
    {

      $data = array('error 1','error 2','error 3', 'error 4','error 5');

      return $data;
    }

    public function collect($params){

      $dataInput = array();

      $dataInput['wastepointid'] = $params['wastepointid'];
      //$dataInput['productid'] = $params['productid'];
      $dataInput['status'] = 'kosong';
      $dataInput['errorid'] = $params['errorid'] ?? '';
      $dataInput['start_date'] = date('Y-m-d H:i:s');
      $dataInput['total_hours'] = 0;
      $dataInput['isFull'] = 0;
      $dataInput['created_by'] = $params['userid'];

      $where = array(
        'wastepointid' =>$params['wastepointid'],
        'status' => 'kosong'
      );

      $checkData = $this->checkdata($where);

      if(empty($checkData)){

        $simpan = Wastes::create($dataInput);

      }else{

        $simpan = false;

      }

      

      return $simpan;
    }

    public function full($params)
    {

      $dataInput['status'] = 'full';
      $dataInput['errorid'] = $params['errorid'];
      $dataInput['finish_date'] = date('Y-m-d H:i:s');
      $dataInput['total_hours'] = 2;
      $dataInput['isFull'] = 1;
      $dataInput['finished_by'] = $params['userid'];


      $where = array(
        'wastepointid' =>$params['wastepointid'],
        array('isFull','!=','1'),
      );

      $checkData = $this->checkdata($where);

      if(!empty($checkData)){
        $update = Wastes::where('wastepointid',$params['wastepointid'])->update($dataInput);
      }else{
        $update = false;
      }
      

      return $update;
    }

}