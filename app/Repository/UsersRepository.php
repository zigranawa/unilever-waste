<?php

namespace App\Repository;

use App\Repository\UsersInterface;

use Illuminate\Support\Str;
use App\User;


class UsersRepository implements UsersInterface
{
    
    public function lists($where){

        $data = User::select('*')->where($where)->orderBy('id', 'desc')->get();

        return $data;

    }

    public function simpan($params){

        $tambah = User::create($params);

        return $tambah;

    }

    public function show($id){

        $data = User::find($id);

        return $data;

    }

    public function edit($params,$id){

        $update = User::where('id',$id)->update($params);
        
        return $update;

    }

    public function hapus($id) {

        $params = array('Aktif'=> 0);
        $delete = User::where('id',$id)->update($params);

        return $delete;
    }


}