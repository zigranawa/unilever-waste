<?php

namespace App\Repository;

use App\Repository\WastepointInterface;

use Illuminate\Support\Str;

use App\models\Wastepoint;
use App\models\Mesin;

class WastepointRepository implements WastepointInterface
{
    
    public function lists($where){

        $data = Wastepoint::select('*')->where($where)->orderBy('id', 'desc')->get();

        foreach ($data as $key => $value) {
            $mesin = $value->mesin();

            $data[$key]->nama_mesin = $mesin->nama_mesin ?? '';

        }

        return $data;


    }
    
    public function simpan($params){

        $tambah = Wastepoint::create($params);

        return $tambah;

    }
    
    public function show($id){

        $data = Wastepoint::find($id);
        $mesin = $data->mesin();

        $data->nama_mesin = $mesin->nama_mesin;

        return $data;

    }
    
    public function edit($params,$id){

        $update = Wastepoint::where('id',$id)->update($params);
        
        return $update;

    }

     public function hapus($id) {

        $params = array('aktif'=> 0);
        $delete = Wastepoint::where('id',$id)->update($params);

        return $delete;
    }

    public function mesin(){
        $mesin = Mesin::select()->where(array('aktif' => 1))->get();

        return $mesin;
    }
}