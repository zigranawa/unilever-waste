<?php

namespace App\Repository;

use App\Repository\MesinInterface;

use Illuminate\Support\Str;
use App\models\Mesin;


class MesinRepository implements MesinInterface
{
    
    public function lists($where){

        $data = Mesin::select('*')->where($where)->orderBy('id', 'desc')->get();

        return $data;

    }

    public function simpan($params){

        $tambah = Mesin::create($params);

        return $tambah;

    }

    public function show($id){

        $data = Mesin::find($id);

        return $data;

    }

    public function edit($params,$id){

        $update = Mesin::where('id',$id)->update($params);
        
        return $update;

    }

    public function hapus($id) {

        $params = array('aktif'=> 0);
        $delete = Mesin::where('id',$id)->update($params);

        return $delete;
    }


}