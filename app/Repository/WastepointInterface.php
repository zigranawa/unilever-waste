<?php

namespace App\Repository;

Interface WastepointInterface {
    public function lists($params);
    public function simpan($params);
    public function show($id);
    public function edit($params,$id);
    public function hapus($id);
}