<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWastesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wastes', function (Blueprint $table) {
            $table->id();
            $table->integer('wastepointid');
            $table->integer('productid')->nullable();
            $table->string('status');
            $table->string('errorid');
            $table->date('start_date');
            $table->date('finish_date')->nullable();
            $table->string('total_hours')->nullable();
            $table->integer('isFull')->nullable();
            $table->integer('created_by');
            $table->integer('finished_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wastes');
    }
}
