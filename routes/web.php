<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function ($router) {
    
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/report', function () {
	    return view('admin.pages.report.lists');
	})->name('report.index');

    Route::resource('/user', 'admin\UserController');
    Route::resource('/product', 'admin\ProductController');
    Route::resource('/mesin', 'admin\MesinController');
    Route::resource('/wastepoint', 'admin\WastepointController');


});
