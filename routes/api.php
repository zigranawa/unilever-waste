<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'api\user\UserController@login');
Route::post('register', 'api\user\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){

	Route::group(['prefix' => 'user'], function ($router) {
		Route::post('details', 'api\user\UserController@details');
	});
	
	Route::group(['prefix' => 'scan'], function ($router) {
		Route::post('lists', 'api\scan\WastebinController@lists');

		Route::post('check', 'api\scan\WastebinController@check');

	});

  Route::post('collect-waste', 'api\scan\WastebinController@collect');

  Route::post('full-waste', 'api\scan\WastebinController@full');

  Route::post('error/lists', 'api\scan\WastebinController@errorlist');
});