<!-- Sidebar  -->
<nav id="sidebar">
	<div class="sidebar_blog_1">
		<div class="sidebar-header">
			<div class="logo_section">
				<a href="index.html"><img class="logo_icon img-responsive" src="/assets/images/logo/logos.png" alt="#" /></a>
			</div>
		</div>
		<div class="sidebar_user_info">
			<div class="icon_setting"></div>
			<div class="user_profle_side">
				<div class="user_img"><img class="img-responsive" src="/assets/images/layout_img/user_img.jpg" alt="#" /></div>
				<div class="user_info">
					<h6>{{ Auth::user()->name }}</h6>
					<p><span class="online_animation"></span> Online</p>
				</div>
			</div>
		</div>
	</div>
	<div class="sidebar_blog_2">
		<h4>General</h4>
		<ul class="list-unstyled components">
			
			<li><a href="/admin/home"><i class="fa fa-dashboard yellow_color"></i> <span>Dashboard</span></a></li>
			<li><a href="{{ route('user.index') }}"><i class="fa fa-user orange_color"></i> <span>Users</span></a></li>
			<li>
				<a href="#element" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-diamond purple_color"></i> <span>Master</span></a>
				<ul class="collapse list-unstyled" id="element">
					<li><a href="{{ route('mesin.index') }}"> <span>Mesin</span></a></li>
					<li><a href="{{ route('wastepoint.index') }}"> <span>Waste Point</span></a></li>
				</ul>
			</li>

			<li><a href="{{ route('product.index') }}"><i class="fa fa-briefcase blue1_color"></i> <span>Product</span></a></li>

			<li><a href="{{ route('report.index') }}"><i class="fa fa-table purple_color2"></i> <span>Laporan</span></a></li>
			
			</ul>
		</div>
	</nav>
            <!-- end sidebar -->