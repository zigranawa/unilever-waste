@extends('admin.layouts.header')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="row column_title">
		<div class="col-md-12">
			<div class="page_title">
				<h2>Edit Waste Point</h2>
			</div>
		</div>
	</div>
	<!-- row -->
	<div class="row">

		<div class="col-md-12">
			
			<div class="white_shd full margin_bottom_30">
				<div class="full graph_head">
					<div class="heading1 margin_0">
						
						<a href="{{ route('wastepoint.index') }}" class="btn btn-sm btn-success float-right" type="button">
						<i class="fa fa-arrow-left"></i> Kembali
					</a>
					</div>
				</div>
				<div class="table_section padding_infor_info">

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		           	@endif
				    <form action="{{ route('wastepoint.update',$data->id) }}" method="POST">
				        @csrf
				        @method('PUT')
				   
				         <div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Nama:</strong>
				                    <input type="text" name="nama" value="{{ $data->nama }}" class="form-control" placeholder="nama">
				                </div>
				            </div>
				            
				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Nama Mesin:</strong>
				                     <select class="form-control" name="mesin">

				                    	<option value="">Pilih</option>

				                    	@foreach($data->mesin as $key=>$value)
				                    		<option value="{{ $value['id'] }}" {{ $data->mesinid ==  $value['id'] ? 'selected':'' }}>
				                    			{{ $value['nama_mesin'] }}
				                    		</option>
				                    	@endforeach
				                    </select>
				                </div>
				            </div>
				           	<div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Kode:</strong>
				                   <input type="text" name="code" value="{{ $data->code }}" class="form-control" placeholder="code">
				                </div>
				            </div>
				            
				            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
				              <button type="submit" class="btn btn-primary">SIMPAN</button>
				            </div>
				        </div>
				   
				    </form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- /.container-fluid -->

@endsection

@section('addingScriptJs')


@endsection