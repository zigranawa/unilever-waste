@extends('admin.layouts.header')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="row column_title">
		<div class="col-md-12">
			<div class="page_title">
				<h2>Detail Mesin</h2>
			</div>
		</div>
	</div>
	<!-- row -->
	<div class="row">

		<div class="col-md-12">
			
			<div class="white_shd full margin_bottom_30">
				<div class="full graph_head">
					<div class="heading1 margin_0">
						
						<a href="{{ route('mesin.index') }}" class="btn btn-sm btn-success float-right" type="button">
						<i class="fa fa-arrow-left"></i> Kembali
					</a>
					</div>
				</div>
				<div class="table_section padding_infor_info">

					<div class="row">
			            <div class="col-xs-12 col-sm-12 col-md-12">
			                <div class="form-group">
			                    <strong>Nama:</strong>
			                    {{ $data->nama_mesin }}
			                </div>
			            </div>

			            <div class="col-xs-12 col-sm-12 col-md-12">
		                <div class="form-group">
		                    <strong>Kode:</strong>
		                    {{ $data->code }}
		                </div>
		            </div>

		            <div class="col-xs-12 col-sm-12 col-md-12">
		                <div class="form-group">
		                    <strong>Nama mesin:</strong>
		                    {{ $data->nama_mesin }}
		                </div>
		            </div>
		        	</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('addingScriptJs')


@endsection