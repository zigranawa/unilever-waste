@extends('admin.layouts.header')

@section('content')

<div class="container-fluid">
	<div class="row column_title">
		<div class="col-md-12">
			<div class="page_title">
				<h2>Product</h2>
			</div>
		</div>
	</div>
	<!-- row -->
	<div class="row">
		<!-- table section -->
		<div class="col-md-12">

			@if ($message = Session::get('success'))
		        <div class="alert alert-success">
		            <p>{{ $message }}</p>
		        </div>
		    @endif


			<div class="white_shd full margin_bottom_30">
				<div class="full graph_head">
					<div class="heading1 margin_0">
						<h2>Data Product</h2>
					</div>

					<a class="btn btn-default pull-right" href="{{ route('product.create') }}">
		            	<i class="fa fa-plus"></i> Tambah
		            </a>
				</div>


				<div class="table_section padding_infor_info">

					<div class="table-responsive-sm">
						<table class="table">
							<thead>
								<tr>
									<th>id</th>
									<th>Nama Products</th>
									<th>Nama Waste Point</th>
									<th>Kode Product</th>
									<th>Harga</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data as $key=>$value)
								<tr>
									<td>{{ $value->id }}</td>
									<td>{{ $value->nama }}</td>
									<td>{{ $value->nama_wastepoint }}</td>
									<td>{{ $value->code }}</td>
									<td>{{ $value->harga }}</td>
									<td>
										<form action="{{ route('product.destroy',$value->id) }}" method="POST">
						                    @csrf
						                    @method('DELETE')


						                    <a class="btn btn-warning btn-icon btn-transparent-dark" href="{{ route('product.edit',$value->id) }}">
						                    	<i class="fa fa-pencil"></i>
						                    </a>

						                    <a class="btn btn-success btn-icon btn-transparent-dark" href="{{ route('product.show',$value->id) }}">
						                    	<i class="fa fa-eye"></i>
						                    </a>
						      
						                    <button type="submit" class="btn btn-danger btn-icon btn-transparent-dark">
												<i class="fa fa-trash-o"></i>
											</button>

						                </form>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection