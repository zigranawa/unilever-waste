@extends('admin.layouts.header')

@section('content')

<!-- Begin Page Content -->

<div class="container-fluid">
	<div class="row column_title">
		<div class="col-md-12">
			<div class="page_title">
				<h2>Tambah Product</h2>
			</div>
		</div>
	</div>
	<!-- row -->
	<div class="row">

		<div class="col-md-12">
			
			<div class="white_shd full margin_bottom_30">
				<div class="full graph_head">
					<div class="heading1 margin_0">
						
						<a href="{{ route('product.index') }}" class="btn btn-sm btn-success float-right" type="button">
						<i class="fa fa-arrow-left"></i> Kembali
					</a>
					</div>
				</div>
				<div class="table_section padding_infor_info">

					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		           	@endif

				    <form action="{{ route('product.update',$data->id) }}" method="POST">
				       	@csrf
				        @method('PUT')
				   
				         <div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Nama Product:</strong>
				                    <input type="text" name="nama" value="{{ $data->nama }}" class="form-control" placeholder="nama">
				                </div>
				            </div>

				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Kode Product:</strong>
				                    <input type="text" name="code" value="{{ $data->code }}" class="form-control" placeholder="code">
				                </div>
				            </div>

				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Nama Waste Point:</strong>
				                   
				                    <select class="form-control" name="wastepoint">

				                    	<option value="">Pilih</option>

				                    	@foreach($data['wastepoint'] as $key=>$value)
				                    		<option value="{{ $value['id'] }}" {{ $data->wastepointid ==  $value['id'] ? 'selected':'' }}>
				                    			{{ $value['nama'] }}
				                    		</option>
				                    	@endforeach
				                    </select>
				                </div>
				            </div>

				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Harga :</strong>
				                    <input type="text" name="harga" value="{{ $data->harga }}" class="form-control" placeholder="harga">
				                </div>
				            </div>
				            
				            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
				              <button type="submit" class="btn btn-primary">SIMPAN</button>
				            </div>
				        </div>
				   
				    </form>
				</div>
			</div>

		</div>
		
	</div>

</div>

<!-- /.container-fluid -->

@endsection

@section('addingScriptJs')


@endsection