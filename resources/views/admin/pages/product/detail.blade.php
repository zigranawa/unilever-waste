@extends('admin.layouts.header')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<h1 class="h3 mb-2 text-gray-800">CABANG</h1>
	<p class="mb-4"></p>

	<!-- DataTales Example -->
	<div class="card shadow mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 col-sm-6 font-weight-bold text-primary float-left">DETAIL CABANG</h6>


			<a href="{{ route('cabang.index') }}" class="btn btn-sm btn-success float-right" type="button">
				<i class="fas fa-arrow-left"></i> Kembali
			</a>
			
		</div>
		<div class="card-body">
			
	         <div class="row">
	            <div class="col-xs-12 col-sm-12 col-md-12">
	                <div class="form-group">
	                    <strong>Nama cabang:</strong>
	                    {{ $data->nama_cabang }}
	                </div>
	            </div>
	            
	            <div class="col-xs-12 col-sm-12 col-md-12">
	                <div class="form-group">
	                    <strong>phone:</strong>
	                    {{ $data->telepon }}
	                </div>
	            </div>
	           	<div class="col-xs-12 col-sm-12 col-md-12">
	                <div class="form-group">
	                    <strong>Alamat:</strong>
	                    {{ $data->alamat }}
	                </div>
	            </div>
	            
	        </div>
		   
		</div>
	</div>

</div>
<!-- /.container-fluid -->

@endsection

@section('addingScriptJs')


@endsection