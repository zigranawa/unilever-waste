@extends('admin.layouts.header')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">
	<div class="row column_title">
		<div class="col-md-12">
			<div class="page_title">
				<h2>Tambah User</h2>
			</div>
		</div>
	</div>
	<!-- row -->
	<div class="row">

		<div class="col-md-12">
			
			<div class="white_shd full margin_bottom_30">
				<div class="full graph_head">
					<div class="heading1 margin_0">
						
						<a href="{{ route('user.index') }}" class="btn btn-sm btn-success float-right" type="button">
						<i class="fa fa-arrow-left"></i> Kembali
					</a>
					</div>
				</div>
				<div class="table_section padding_infor_info">



					@if (count($errors) > 0)
		                <div class="alert alert-danger">
		                    <ul>
		                        @foreach ($errors->all() as $error)
		                            <li>{{ $error }}</li>
		                        @endforeach
		                    </ul>
		                </div>
		           	@endif
				    <form action="{{ route('user.store') }}" method="POST">
				        @csrf
				        @method('POST')
				   
				         <div class="row">
				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>Nama:</strong>
				                    <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="">
				                </div>

				                <div class="form-group">
				                    <strong>Email:</strong>
				                    <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="">
				                </div>

				                <div class="form-group">
				                    <strong>Role:</strong>
				                    <select class="form-control" name="role">

				                    	<option>Pilih</option>

				                    	<option value="1" {{ old('old') == 1 ? 'selected' : '' }}> Admin </option>
				                    	<option value="2" {{ old('old') == 2 ? 'selected' : '' }}> Admin Staff </option>
				                    	<option value="3" {{ old('old') == 3 ? 'selected' : '' }}> Admin rework </option>
				                    </select>
				                </div>
				            </div>

				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>password:</strong>
				                    <input type="password" id="Password" name="password" value="" class="form-control" placeholder="">
				                </div>
				            </div>
				            <div class="col-xs-12 col-sm-12 col-md-12">
				                <div class="form-group">
				                    <strong>konfirmasi password :</strong>
				                    <input type="password" id="RePassword" name="konfirmasi_password" value="" class="form-control" placeholder="">
				                </div>
				            </div>
				            
				            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
				              <button type="submit" class="btn btn-primary">SIMPAN</button>
				            </div>
				        </div>
				   
				    </form>
				</div>
			</div>

		</div>
		
	</div>

</div>
<!-- /.container-fluid -->

@endsection

@section('addingScriptJs')


@endsection